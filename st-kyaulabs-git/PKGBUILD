# Maintainer: kyau <kyau@kyau.net>

pkgname=st-kyaulabs-git
_pkgname=st
patchname=kyaulabs
patchver=0.8.2
pkgver=0.8.2.r25.g3848301
pkgrel=1
pkgdesc='Simple virtual terminal emulator for X (KYAU Labs Edition)'
arch=('i686' 'x86_64')
url='https://st.suckless.org/'
license=('MIT')
depends=('libxft' 'xorg-fonts-misc')
makedepends=('ncurses' 'libxext' 'git')
optdepends=('dmenu: for unicode input')
provides=('st')
conflicts=('st')

_patches=(
	"https://st.suckless.org/patches/vertcenter/st-vertcenter-20180320-6ac8c8a.diff"
	"st-blinking-cursor-20180605.diff"
	"https://st.suckless.org/patches/scrollback/st-scrollback-20190331-21367a0.diff"
	"https://st.suckless.org/patches/scrollback/st-scrollback-mouse-20191024-a2c479c.diff"
)

source=("git://git.suckless.org/st"
	"config.h"
	"${_patches[@]}")
	
md5sums=('SKIP'
		 'b21b92b70fb07f7f26c0b5cbec04eab6'
		 '51106ec8ff04d64029401421bbc57ab5'
		 'e72ba1e6c08319c397678a285f4d066f'
		 '4918037063b759c930f312debe869941'
		 'f2569d451cd3e9e4f2e12cefe6ee5bd8'
		 )

provides=("${_pkgname}")
conflicts=("${_pkgname}")

pkgver() {
	cd "${_pkgname}"
	git describe --long --tags | sed 's/\([^-]*-g\)/r\1/;s/-/./g'
}

prepare() {
	local file
	cd "${_pkgname}"

	# skip terminfo which conflicts with nsurses
	sed -i '/tic /,+1d' Makefile

	for file in "${_patches[@]}"; do
		if [[ "$file" == *.h ]]; then
			cp "$srcdir/$file" .
		elif [[ "$file" == *.diff || "$file" == *.patch ]]; then
			echo "Applying patch $(basename $file)..."
			patch -Np1 <"$srcdir/$(basename ${file})"
		fi
	done
	cp -f $srcdir/config.h config.h
}

build() {
	cd "${_pkgname}"

	make X11INC=/usr/include/X11 X11LIB=/usr/lib/X11
}

package() {
	cd "${_pkgname}"
	make PREFIX=/usr DESTDIR="${pkgdir}" TERMINFO="$pkgdir/usr/share/terminfo" install
	install -Dm644 LICENSE "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
	install -Dm644 README "${pkgdir}/usr/share/doc/${pkgname}/README"
}

# vim: ft=PKGBUILD ts=4 sw=4 noet:
